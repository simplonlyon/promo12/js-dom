// document.querySelector("#para2").style.color = "blue";

let para2 = document.querySelector('#para2');
para2.style.color = 'blue';

let section2 = document.querySelector('#section2');
section2.style.border = '1px dotted black';

let colorfulSect2 = document.querySelector('#section2 .colorful');
colorfulSect2.style.backgroundColor = 'orange';

let h2Sect1 = document.querySelector('#section1 h2');
h2Sect1.style.fontStyle = 'italic';

let colorfulP = document.querySelector('p .colorful');
colorfulP.style.display = 'none';

para2.textContent = 'modified by JS';


let link = document.querySelector('#section1 a');
link.href = 'https://www.simplonlyon.fr';

let h2Sect2 = document.querySelector('#section2 h2');
h2Sect2.classList.add('big-text');
// h2Sect2.className += ' big-text'; //Moins bien

//querySelectorAll renvoie un tableau d'element qui matchent le selecteur
let paras = document.querySelectorAll('p');
//Pour appliquer une modification à tous ces éléments, on doit itérer dessus avec une boucle
for (const item of paras) {
    //Pour chaque item de la boucle, on applique la modification voulue
    item.style.fontStyle = 'italic';
    
}