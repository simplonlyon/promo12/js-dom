/**
 * Ici, on a tous nos petits éléments html capturés (c'est pas
 * une obligation absolu de les regrouper comme ça, mais faut au moins
 * faire en sorte de les déclarer à des endroits logiques histoire
 * qu'on passe pas notre temps à chercher nos déclarations de variables
 * quand on a un doute sur leur contenu)
 */
let colList = document.querySelectorAll('.exo-col');
let selectJustify = document.querySelector('#select-justify');
let selectAlign = document.querySelector('#select-align');
let row = document.querySelector('.row');
let colInput = document.querySelector('#col-input');
let showInput = document.querySelector('#show-input');

/**
 * On fait une première boucle au lancement sur toutes les colonnes pour
 * mettre une classe bootstrap col-4 dessus
 */
for (const col of colList) {
    
    col.classList.add('col-4');
}

//On crée une variable qui contient la dernière valeur du select
let currentJustify = 'justify-content-between';
//On met un event au change sur le select
selectJustify.addEventListener('change', function () {
    /**
     * Ici, on utilise la variable qui contient notre élément row
     * et on fait en sorte de remplacer dans la liste de ses classe
     * la dernière valeur de l'input par la nouvelle valeur de l'input
     */
    row.classList.replace(currentJustify, selectJustify.value);
    //On met la nouvelle valeur de l'input dans la variable
    currentJustify = selectJustify.value;



});
//Pareil qu'au dessus mais pour le align-items
let currentAlign = 'align-items-start';
selectAlign.addEventListener('change', function () {

    row.classList.replace(currentAlign, selectAlign.value);

    currentAlign = selectAlign.value;

});
//Pareil mais pour les classes de col-x
let currentCol = 'col-4';
colInput.addEventListener('change', function() {
    //à la différence qu'on doit faire une boucle car on a plusieurs
    //élément dont on veut modifier la classe
    for (const col of colList) {
        //et au fait qu'on doivent faire une concaténation du fait
        //qu'on utilise une input de type number (on aurait pu
        //utiliser un select aussi, mais c'était pour changer)
        col.classList.replace(currentCol, 'col-'+colInput.value);
    }
    currentCol = 'col-'+colInput.value;    

});

showInput.addEventListener('change', function() {
    /**
     * Pour l'affichage des éléments, on fait une boucle classique
     * afin de pouvoir avoir l'index de la boucle sous la main
     */
    for (let index = 0; index < colList.length; index++) {
        //On récupère quand même notre élément colonne
        const col = colList[index];
        /**
         * On fait une condition pour faire que tant que l'index est
         * inférieur à la valeur actuelle de l'input, on display block
         * l'élément, sinon on le display none pour le cacher
         */
        if(index < showInput.value) {
            col.style.display = 'block';
        } else {
            col.style.display = 'none';
        }

    }
    
});