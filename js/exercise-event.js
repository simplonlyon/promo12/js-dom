let firstBtn = document.querySelector('#first-btn');

firstBtn.addEventListener('click', function() {
    console.log('bloup');
});

//Counter
let count = 0;
let counterElement = document.querySelector('#counter');
displayCount();

let plusBtn = document.querySelector('#plus-btn');
plusBtn.addEventListener('click', function() {
    count++;
    displayCount();
});

let minusBtn = document.querySelector('#minus-btn');
minusBtn.addEventListener('click', function() {
    count--;
    displayCount();
});

let resetBtn = document.querySelector('#reset-btn');
resetBtn.addEventListener('click', function() {
    count = 0;
    displayCount();
});

function displayCount() {
    counterElement.textContent = count;
}


/* Bonus ché-per

let counterBtnList = document.querySelectorAll('button');
for(let btn of counterBtnList) {
    btn.addEventListener('click', function() {
       count += Number(btn.getAttribute('data-count'));
        displayCount();
    });
}

function displayCount() {
    let resetBtn = document.querySelector('#reset-btn');
    resetBtn.setAttribute('data-count', -count);
    counterElement.textContent = count;
}
*/