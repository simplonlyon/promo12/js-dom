# js-dom

#Exercice DOM
1. Mettre le texte de l'élément avec l'id para2 en bleu
2. Mettre une border en pointillet noire à la section2
3. Mettre une background color orange à l'élément de la classe colorful de la section2 
4. Mettre le h2 de la section1 en italique
5. Cacher l'élément colorful situé dans un paragraphe
6. Changer le texte de para2 pour "modified by JS"
7. Changer le href du lien de la section1 pour le faire aller sur simplonlyon.fr
8. Rajouter la classe big-text sur le h2 de la section2
9. Bonus : Faire que tous les paragraphe du document soit en italique


## Exercice Compteur
Faire un petit compteur avec un bouton + et un bouton - qui va modifier la valeur du compteur et l'afficher
1. Créer une variable count initiliasée à 0
2. Dans le HTML, rajouter un élément button avec un + dedans et un avec un - dedans, pourquoi pas leur mettre un id
3. Dans le js, capturer le bouton +, puis lui rajouter un event listener au click (mettre un console log dedans par exemple pour voir si déjà ça c'est ok)
4. Dans cet event listener, faire en sorte d'incrémenter la variable count puis en faire un console log
5. Répéter l'étape 3 et 4 pour le bouton - mais cette fois ci faire une décrémentation (à cette étape là, le compteur fonctionne sur le principe, il reste plus que l'affichage à gérer)
6. Dans le HTML rajouter une balise span entre les deux bouton avec un ptit id et le capturer dans le js
7. Dans les event listener de chaque bouton, faire en sorte de changer le textContent du span par la valeur actuelle de count
8. Bonus 1 : Faire un bouton reset qui remet le compteur à zéro
9. Bonus 2 (plus compliqué) : Tenter de refactoriser pour avoir la même fonction sur les deux event listener du + et du -

## Exercice Grid Cheatsheet
### I. Mise en place
1. Créer 2 fichier grid-cheatsheet.html et .js dans le projet js-dom
2. Dans le fichier HTML, faire le lien vers le js et aussi rajouter le link du css de bootstrap (pas besoin du js de bootstrap pour ce truc)
3. Dans le HTML toujours, créer une div container-fluid avec une div row à l'intérieur, et dans la row, créer 12 div avec une classe exo-col et avec un article à l'intérieur
Rajouter aussi une classe border sur les articles et du texte dedans genre "article"
4. Dans le fichier js, faire une selection de tous les élément .exo-col et faire en sorte de leur rajouter la classe "col-4" avec une boucle
### II. Les selects de la row
1. Dans le html, rajouter une balise select qui aura comme options les différentes classes justify (justify-content-start, justify-content-center, justify-content-end, justify-content-around, justify-content-between)
2. Capturer l'élément select dans le javascript, et faire qu'à l'event change de cet élément, cela rajoute la classe actuellement séléctionnée (la value de l'élément) aux classes de la div.row
3. Actuellement, dès qu'on change le select, une nouvelle classe se rajoute sur la row, on a donc une row avec plusieurs justifty-content-x, essayer de trouver un moyen de faire qu'elle ne puisse en avoir qu'un seul à la fois (je donne une solution possible en spoiler en dessous)
<details><summary>Indice</summary>
<p>
Créer une variable currentJustify initialisée vide, et faire que la valeur de cette variable soit modifié lors de l'event change du select. Puis juste au dessus de cette assignation dans l'event, faire un classList.remove() de la variable afin de retirer la classe précédente
</p>
</details>

4. Répéter les étapes 1,2,3 avec un select pour les align-items
### III. Changer le nombre de colonnes
1. Dans le HTML créer une balise input type number, et la capturer dans le js (lui mettre un max et un min pour qu'il puisse aller que de 1 à 12)
2. Rajouter un event sur cet input qui assignera à tous les .exo-col la classe col-x où x est la value actuelle de l'input
3. Utiliser le même truc que dans le II. 3. pour faire que chaque .exo-col ne puisse avoir qu'un seul col-x à la fois
### IV. Changer le nombre d'éléments affichés
1. Dans le HTML, créer une balise input number et la capturer dans le js, et lui mettre un min max 1-12, comme avant
2. Rajouter un event de type input sur cet élément qui fera une boucle for classique sur tous les .exo-col
3. Dans la boucle, faire un display:block sur chaque exo-col si la valeur de la boucle (x) est inférieure à la value de l'input, sinon faire un display:none